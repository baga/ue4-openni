using UnrealBuildTool;
using System.Collections.Generic;

public class NiDemoTarget : TargetRules
{
	public NiDemoTarget(TargetInfo Target)
	{
		Type = TargetType.Game;
	}

	public override void SetupBinaries(
		TargetInfo Target,
		ref List<UEBuildBinaryConfiguration> OutBuildBinaryConfigurations,
		ref List<string> OutExtraModuleNames
		)
	{
		OutExtraModuleNames.AddRange( new string[] { "NiDemo" } );
	}
}
