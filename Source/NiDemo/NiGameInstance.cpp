#include "NiDemo.h"
#include "NiGameInstance.h"
#include "NiSensor.h"

void UNiGameInstance::Init()
{
	UGameInstance::Init();

	UE_LOG(LogTemp, Warning, TEXT("UNiGameInstance::Init"));

	//UNiSensor::GetMotionSensor()->StartTracking();
}

void UNiGameInstance::Shutdown()
{
	UE_LOG(LogTemp, Warning, TEXT("UNiGameInstance::Shutdown"));

	//UNiSensor::GetMotionSensor()->StopTracking();

	UGameInstance::Shutdown();
}
