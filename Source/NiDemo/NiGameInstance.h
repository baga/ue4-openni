#pragma once

#include "Engine/GameInstance.h"
#include "NiGameInstance.generated.h"

UCLASS()
class NIDEMO_API UNiGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	virtual void Init();
	virtual void Shutdown();
};
