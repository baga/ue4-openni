using System.IO;
using UnrealBuildTool;

public class NiDemo : ModuleRules
{
	public NiDemo(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] {"Core", "CoreUObject", "Engine", "InputCore"});
		PrivateDependencyModuleNames.AddRange(new string[] {});

		PrivateDependencyModuleNames.AddRange(new string[] { "NaturalInteraction" });
		PrivateIncludePathModuleNames.AddRange(new string[] { "NaturalInteraction" });

		//PublicIncludePaths.Add(Path.Combine(ThirdPartyPath, "OpenNI/include/"));
		//PublicAdditionalLibraries.Add(Path.Combine(ThirdPartyPath, "OpenNI/lib/OpenNI2.lib"));
		//PublicAdditionalLibraries.Add(Path.Combine(ThirdPartyPath, "OpenNI/lib/NiTE2.lib"));

		//PublicIncludePaths.Add(Path.Combine(PluginsPath, "NaturalInteraction/Source"));

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");
		// if ((Target.Platform == UnrealTargetPlatform.Win32) || (Target.Platform == UnrealTargetPlatform.Win64))
		// {
		//		if (UEBuildConfiguration.bCompileSteamOSS == true)
		//		{
		//			DynamicallyLoadedModuleNames.Add("OnlineSubsystemSteam");
		//		}
		// }
	}

	private string ThirdPartyPath
	{
		get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../ThirdParty/")); }
	}

	private string PluginsPath
	{
		get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../../Plugins/")); }
	}
}
