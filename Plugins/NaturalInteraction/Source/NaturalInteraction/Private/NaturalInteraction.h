#pragma once

#include "INaturalInteraction.h"

class FNaturalInteraction : public INaturalInteraction
{
public:

	FNaturalInteraction();
	virtual ~FNaturalInteraction();

	// IModuleInterface
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	// INaturalInteraction
	virtual class FNiContext* GetContext();

private:

	void InitContext();
	void ReleaseContext();

private:

	class FNiContext* Context;
};
