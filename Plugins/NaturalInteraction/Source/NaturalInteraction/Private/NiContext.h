#pragma once

#include "AllowWindowsPlatformTypes.h"
#include "NiTE.h"
#include "HideWindowsPlatformTypes.h"

#include "NiSensor.h"
#include "NiUtils.h"

struct FTextureUpdateData;

class FNiContext : public FRunnable
{
public:

	FNiContext();
	virtual ~FNiContext();

	// FRunnable
	virtual bool Init() { return true; }
	virtual uint32 Run();
	virtual void Stop() { ThreadFlag = 0; }

	UNiSensor* GetSensor();
	void ResetSensor();

	bool InitDevice();
	void ShutdownDevice();
	bool IsDeviceReady() const;

	bool StartTracking();
	void StopTracking();

	bool StartPoseDetection(UNiUser* User, ENiPoseType Pose);
	void StopPoseDetection(UNiUser* User, ENiPoseType Pose);

	UTexture2D* GetDepthTexture() { return DepthTexture; }

private:

	bool UpdateDepth(nite::UserTrackerFrameRef& Frame);
	void ComputeDepthHist(openni::VideoFrameRef& Frame);
	void FillDepthBuffer(nite::UserTrackerFrameRef& Frame, openni::VideoFrameRef& DepthFrame);

	bool UpdateUsers(nite::UserTrackerFrameRef& Frame);
	void UpdateSkeleton(UNiSensor* Sensor, const nite::UserData& User, UNiUser* UserObj);
	void UpdatePose(UNiSensor* Sensor, const nite::UserData& User, UNiUser* UserObj);

private:

	FCriticalSection SensorMx;
	FCriticalSection DeviceMx;

	UNiSensor* SensorInstance;
	openni::Device* Device;
	nite::UserTracker* UserTracker;
	int FlagOpenNI;
	int FlagNite;

	UTexture2D* DepthTexture;
	FTextureUpdateData* DepthTextureData;
	TArray<float> DepthHist;
	TArray<uint8_t> DepthBuffer;
	int DepthWidth;
	int DepthHeight;

	FRunnableThread* Thread;
	volatile int ThreadFlag;
	volatile int ThreadExitFlag;
};
