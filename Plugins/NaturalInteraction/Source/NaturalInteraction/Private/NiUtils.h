#pragma once

/*
OpenNI defines two types of coordinate systems:
- Projective Coordinates - a two-dimentional map of depth values
- Real World Coordinates - A collection of three-dimentional points.

Those two coordinate systems are defined as below:

<table>
<tr><th>Feature</th>        <th>Projective Coordinates</th>        <th>Real World Coordinates</th></tr>
<tr><th>X-Y Units</th>      <td>pixels</td>                        <td>millimeters</td></tr>
<tr><th>Z Units</th>        <td>millimeters</td>                   <td>millimeters</td></tr>
<tr><th>X-Y Origin</th>     <td>upper-left corner of the FOV</td>  <td>center of FOV</td></tr>
<tr><th>Z Origin</th>       <td>sensor's plane</td>                <td>sensor's plane</td></tr>
<tr><th>X Direction</th>    <td>left to right</td>                 <td>left to right</td></tr>
<tr><th>Y Direction</th>    <td>top to bottom</td>                 <td>bottom to top</td></tr>
<tr><th>Z Direction</th>    <td>away from sensor</td>              <td>away from sensor</td></tr>
</table>

A main difference between the two is that in projective coordinates, a real-world object (person, chair) gets bigger 
(in pixels) as it gets closer to the sensor, whereas its real-world size remains the same.

@note Some data in OpenNI is returned in projective coordinates (for example, depth maps), and some is returned
in real-world coordinates (for example, skeleton joints). Pay attention when comparing the two.

A depth generator creates the depth value of each pixel. By knowing the distance of the object and some geometrical
information about the sensor (like its FOV), translation can be made between the two systems. see 
@ref xn::DepthGenerator::ConvertProjectiveToRealWorld() and @ref xn::DepthGenerator::ConvertRealWorldToProjective().
*/

class Utils
{
public:

	static ENiJointType ConvertJointType(nite::JointType type)
	{
		switch (type)
		{
			case nite::JOINT_HEAD: return ENiJointType::JOINT_HEAD;
			case nite::JOINT_NECK: return ENiJointType::JOINT_NECK;

			case nite::JOINT_LEFT_SHOULDER: return ENiJointType::JOINT_LEFT_SHOULDER;
			case nite::JOINT_RIGHT_SHOULDER: return ENiJointType::JOINT_RIGHT_SHOULDER;
			case nite::JOINT_LEFT_ELBOW: return ENiJointType::JOINT_LEFT_ELBOW;
			case nite::JOINT_RIGHT_ELBOW: return ENiJointType::JOINT_RIGHT_ELBOW;
			case nite::JOINT_LEFT_HAND: return ENiJointType::JOINT_LEFT_HAND;
			case nite::JOINT_RIGHT_HAND: return ENiJointType::JOINT_RIGHT_HAND;

			case nite::JOINT_TORSO: return ENiJointType::JOINT_TORSO;

			case nite::JOINT_LEFT_HIP: return ENiJointType::JOINT_LEFT_HIP;
			case nite::JOINT_RIGHT_HIP: return ENiJointType::JOINT_RIGHT_HIP;
			case nite::JOINT_LEFT_KNEE: return ENiJointType::JOINT_LEFT_KNEE;
			case nite::JOINT_RIGHT_KNEE: return ENiJointType::JOINT_RIGHT_KNEE;
			case nite::JOINT_LEFT_FOOT: return ENiJointType::JOINT_LEFT_FOOT;
			case nite::JOINT_RIGHT_FOOT: return ENiJointType::JOINT_RIGHT_FOOT;
		}

		return ENiJointType::JOINT_UNKNOWN;
	}

	static ENiPoseType ConvertPoseType(nite::PoseType type)
	{
		switch (type)
		{
			case nite::POSE_PSI: return ENiPoseType::POSE_PSI;
			case nite::POSE_CROSSED_HANDS: return ENiPoseType::POSE_CROSSED_HANDS;
		}

		return ENiPoseType::POSE_UNKNOWN;
	}

	static nite::PoseType ConvertPoseType(ENiPoseType type)
	{
		switch (type)
		{
			case ENiPoseType::POSE_PSI: return nite::POSE_PSI;
			case ENiPoseType::POSE_CROSSED_HANDS: return nite::POSE_CROSSED_HANDS;
		}

		return (nite::PoseType)0;
	}
};
