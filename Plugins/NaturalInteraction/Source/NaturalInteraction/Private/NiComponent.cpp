#include "NiPCH.h"
#include "NiComponent.h"

#include "NaturalInteraction.h"
#include "NiContext.h"
#include "NiLog.h"

UNiComponent::UNiComponent(const FObjectInitializer &ObjectInitializer) : 
	UActorComponent(ObjectInitializer),
	Context(nullptr)
{
	bWantsInitializeComponent = true;
	bAutoActivate = true;
	PrimaryComponentTick.bCanEverTick = true;
}

UNiComponent::~UNiComponent()
{
}

void UNiComponent::OnRegister()
{
	Super::OnRegister();
	Context = INaturalInteraction::Get().GetContext();
}

void UNiComponent::OnUnregister()
{
	Super::OnUnregister();
}

void UNiComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}
