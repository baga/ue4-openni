#include "NiPCH.h"
#include "NiContext.h"

#include "NaturalInteraction.h"
#include "NiLog.h"
#include <chrono>

#define MAX_DEPTH 10000

#define XN_GUARD_BREAK(str, op)\
{\
	const int status = op;\
	if (status != openni::STATUS_OK)\
	{\
		const char* err = openni::OpenNI::getExtendedError();\
		UE_LOG(LogNiPlugin, Error, TEXT("%s failed: Status=%d Error=%s"), TEXT(str), (int)status, ANSI_TO_TCHAR(err));\
		break;\
	}\
}

struct FPixel_R8G8B8A8
{
	uint8_t R, G, B, A;
};

struct FTextureUpdateData
{
	FTexture2DResource* Texture2DResource;
	int32 MipIndex;
	uint32 NumRegions;
	FUpdateTextureRegion2D Region;
	uint32 SrcPitch;
	uint32 SrcBpp;
	uint8* SrcData;
};

template<class T>
inline void SafeDelete(T*& ptr) { if (ptr) { delete ptr; ptr = nullptr; } }

//=============================================================================
// FNiContext
//=============================================================================

FNiContext::FNiContext() : 
	SensorInstance(nullptr),
	Device(nullptr),
	UserTracker(nullptr),
	FlagOpenNI(0),
	FlagNite(0),

	DepthTexture(nullptr),
	DepthTextureData(nullptr),
	DepthWidth(0),
	DepthHeight(0),

	Thread(nullptr),
	ThreadFlag(0)
{
	UE_LOG(LogNiPlugin, Warning, TEXT("NEW FNiContext"));

	DepthTextureData = new FTextureUpdateData;
	FMemory::Memset(DepthTextureData, 0, sizeof(FTextureUpdateData));

	DepthHist.Init(0.0f, MAX_DEPTH);
}

FNiContext::~FNiContext()
{
	UE_LOG(LogNiPlugin, Warning, TEXT("DEL FNiContext"));

	ShutdownDevice();
	SafeDelete(DepthTextureData);
	SensorInstance = nullptr;
}

UNiSensor* FNiContext::GetSensor()
{
	FScopeLock Lock(&SensorMx);

	if (SensorInstance)
	{
		if (SensorInstance->IsValidLowLevel())
		{
			return SensorInstance;
		}
		else
		{
			UE_LOG(LogNiPlugin, Warning, TEXT("Invalid SensorInstance"));
		}
	}

	SensorInstance = NewObject<UNiSensor>();
	SensorInstance->Context = this;
	return SensorInstance;
}

void FNiContext::ResetSensor()
{
	FScopeLock Lock(&SensorMx);

	if (SensorInstance && SensorInstance->IsValidLowLevel())
	{
		UE_LOG(LogNiPlugin, Warning, TEXT("Reset SensorInstance"));
		SensorInstance->Users.Empty();
	}
}

//=============================================================================
// Device
//=============================================================================

bool FNiContext::InitDevice()
{
	UE_LOG(LogNiPlugin, Warning, TEXT("InitDevice"));

	for (;;)
	{
		FScopeLock Lock(&DeviceMx);

		if (Device)
		{
			UE_LOG(LogNiPlugin, Error, TEXT("Already initialized"));
			return false; // do nothing
		}

		try
		{
			XN_GUARD_BREAK("Init OpenNI", openni::OpenNI::initialize());
			FlagOpenNI = 1;

			XN_GUARD_BREAK("Init NiTE", nite::NiTE::initialize());
			FlagNite = 1;

			Device = new openni::Device();
			XN_GUARD_BREAK("Open sensor", Device->open(openni::ANY_DEVICE));

			UserTracker = new nite::UserTracker();
			XN_GUARD_BREAK("Create user tracker", UserTracker->create(Device));
		}
		catch (...)
		{
			UE_LOG(LogNiPlugin, Error, TEXT("InitDevice exception"));
			break;
		}

		return true;
	}

	UE_LOG(LogNiPlugin, Error, TEXT("InitDevice failed"));
	ShutdownDevice();
	return false;
}

void FNiContext::ShutdownDevice()
{
	StopTracking(); // locks device

	UE_LOG(LogNiPlugin, Warning, TEXT("ShutdownDevice"));

	FScopeLock Lock(&DeviceMx);

	SafeDelete(UserTracker);
	SafeDelete(Device);

	if (FlagNite)
	{
		FlagNite = 0;
		UE_LOG(LogNiPlugin, Warning, TEXT("Shutdown NiTE"));
		nite::NiTE::shutdown();
	}

	if (FlagOpenNI)
	{
		FlagOpenNI = 0;
		UE_LOG(LogNiPlugin, Warning, TEXT("Shutdown OpenNI"));
		openni::OpenNI::shutdown();
	}
}

bool FNiContext::IsDeviceReady() const
{
	return (UserTracker && UserTracker->isValid());
}

//=============================================================================
// User tracking
//=============================================================================

bool FNiContext::StartTracking()
{
	UE_LOG(LogNiPlugin, Warning, TEXT("StartTracking"));

	for (;;)
	{
		FScopeLock Lock(&DeviceMx);

		if (!UserTracker)
		{
			UE_LOG(LogNiPlugin, Error, TEXT("Not initialized"));
			return false; // do nothing
		}

		if (Thread || ThreadFlag)
		{
			UE_LOG(LogNiPlugin, Error, TEXT("Already started"));
			return false; // do nothing
		}

		FString name(FString::Printf(TEXT("MotionSensingWorker_%s"), *FGuid::NewGuid().ToString()));

		ThreadFlag = 1;
		Thread = FRunnableThread::Create(this, *name, 0, TPri_AboveNormal);

		if (!Thread)
		{
			UE_LOG(LogNiPlugin, Error, TEXT("Can't create worker thread"));
			break;
		}

		return true;
	}

	UE_LOG(LogNiPlugin, Error, TEXT("StartTracking failed"));
	StopTracking();
	return false;
}

void FNiContext::StopTracking()
{
	UE_LOG(LogNiPlugin, Warning, TEXT("StopTracking"));

	{
		FScopeLock Lock(&DeviceMx);

		ThreadExitFlag = 0;
		ThreadFlag = 0;

		if (Thread)
		{
			UE_LOG(LogNiPlugin, Warning, TEXT("Wait thread"));

			double Timeout = 0;
			while (ThreadExitFlag == 0)
			{
				auto t0 = std::chrono::high_resolution_clock::now();

				FPlatformProcess::Sleep(0);

				auto t1 = std::chrono::high_resolution_clock::now();

				auto Delta = std::chrono::duration_cast<std::chrono::duration<double>>(t1 - t0);
				Timeout += Delta.count();

				if (Timeout > 0.5)
				{
					UE_LOG(LogNiPlugin, Error, TEXT("Wait thread timeout!!!"));
					break;
				}
			}

			if (ThreadExitFlag)
			{
				Thread->Kill(true);
			}
			else
			{
				UE_LOG(LogNiPlugin, Error, TEXT("Terminate thread!!!"));
				Thread->Kill(false);
			}

			delete Thread;
			Thread = nullptr;
		}
	}

	ResetSensor();
}

//=============================================================================
// Pose detection
//=============================================================================

bool FNiContext::StartPoseDetection(UNiUser* User, ENiPoseType Pose)
{
	if (!User)
	{
		UE_LOG(LogNiPlugin, Error, TEXT("Invalid User"));
		return false;
	}

	if (!UserTracker)
	{
		UE_LOG(LogNiPlugin, Error, TEXT("Not initialized"));
		return false;
	}

	const nite::Status Status = UserTracker->startPoseDetection(User->Id, Utils::ConvertPoseType(Pose));

	if (Status != nite::STATUS_OK)
	{
		UE_LOG(LogNiPlugin, Error, TEXT("UserTracker::startPoseDetection failed (Status=%d)"), (int)Status);
		return false;
	}

	return true;
}

void FNiContext::StopPoseDetection(UNiUser* User, ENiPoseType Pose)
{
	if (!User)
	{
		UE_LOG(LogNiPlugin, Error, TEXT("Invalid User"));
		return;
	}

	if (!UserTracker)
	{
		UE_LOG(LogNiPlugin, Error, TEXT("Not initialized"));
		return;
	}

	UserTracker->stopPoseDetection(User->Id, Utils::ConvertPoseType(Pose));
}

//=============================================================================
// Worker thread
//=============================================================================

uint32 FNiContext::Run()
{
	UE_LOG(LogNiPlugin, Warning, TEXT("Enter WorkerThread"));

	uint64 TickCount = 0;
	int NumErrors = 0;

	auto t0 = std::chrono::high_resolution_clock::now();

	while (ThreadFlag)
	{
		nite::UserTrackerFrameRef Frame;
		const nite::Status Status = UserTracker->readFrame(&Frame);
		bool Err = false;

		if (Status != nite::STATUS_OK)
		{
			UE_LOG(LogNiPlugin, Error, TEXT("UserTracker::readFrame failed (Status=%d)"), (int)Status);
			Err = true;
		}

		if (!UpdateDepth(Frame))
		{
			UE_LOG(LogNiPlugin, Error, TEXT("UpdateDepth failed"));
			Err = true;
		}

		if (!UpdateUsers(Frame))
		{
			UE_LOG(LogNiPlugin, Error, TEXT("UpdateUsers failed"));
			Err = true;
		}

		if (Err)
		{
			if (++NumErrors > 5)
			{
				UE_LOG(LogNiPlugin, Error, TEXT("Halt on error counter"));
				break;
			}
		}
		else
		{
			NumErrors = 0;
		}

		++TickCount;
	}

	auto t1 = std::chrono::high_resolution_clock::now();
	auto Delta = std::chrono::duration_cast<std::chrono::duration<double>>(t1 - t0);
	double TicksPerSec = TickCount / Delta.count();

	UE_LOG(LogNiPlugin, Warning, TEXT("Leave WorkerThread (Time=%.2f TicksPerSec=%.2f)"), Delta.count(), TicksPerSec);

	ThreadExitFlag = 1;

	return 0;
}

//=============================================================================
// UpdateDepth
//=============================================================================

bool FNiContext::UpdateDepth(nite::UserTrackerFrameRef& Frame)
{
	openni::VideoFrameRef DepthFrame = Frame.getDepthFrame();

	if (!DepthFrame.isValid())
	{
		return true; // dont care
	}

	const int NewWidth = DepthFrame.getWidth();
	const int NewHeight = DepthFrame.getHeight();

	if (NewWidth < 1 || NewHeight < 1)
	{
		return true; // dont care
	}

	if (!DepthTexture || (DepthWidth != NewWidth) || (DepthHeight != NewHeight))
	{
		UE_LOG(LogNiPlugin, Warning, TEXT("Allocate depth texture: Width=%d Height=%d"), NewWidth, NewHeight);

		// reset resource pointers because render commands executed in other thread
		DepthTextureData->Texture2DResource = nullptr;
		DepthTextureData->SrcData = nullptr;

		DepthWidth = 0;
		DepthHeight = 0;

		// managed by GC
		DepthTexture = UTexture2D::CreateTransient(NewWidth, NewHeight, PF_R8G8B8A8);
		if (!DepthTexture)
		{
			UE_LOG(LogNiPlugin, Error, TEXT("Can't allocate depth texture"));
			return false;
		}

		DepthBuffer.Init(0, NewWidth * NewHeight * sizeof(FPixel_R8G8B8A8));

		DepthWidth = NewWidth;
		DepthHeight = NewHeight;

		ENQUEUE_UNIQUE_RENDER_COMMAND_ONEPARAMETER(
			UpdateDepthTextureCmd,
			UTexture2D*, DepthTexture, DepthTexture,
		{
			DepthTexture->UpdateResource();
		});
	}

	if (DepthTexture->Resource)
	{
		// https://wiki.unrealengine.com/Dynamic_Textures

		ComputeDepthHist(DepthFrame);
		FillDepthBuffer(Frame, DepthFrame);

		DepthTextureData->Texture2DResource = (FTexture2DResource*)DepthTexture->Resource;
		DepthTextureData->MipIndex = 0;
		DepthTextureData->Region = FUpdateTextureRegion2D(0, 0, 0, 0, NewWidth, NewHeight);

		DepthTextureData->SrcPitch = NewWidth * sizeof(FPixel_R8G8B8A8);
		DepthTextureData->SrcBpp = sizeof(FPixel_R8G8B8A8);
		DepthTextureData->SrcData = (uint8*)DepthBuffer.GetData();

		ENQUEUE_UNIQUE_RENDER_COMMAND_ONEPARAMETER(
			UpdateTextureRegionsCmd,
			FTextureUpdateData*, DepthTextureData, DepthTextureData,
		{
			if (DepthTextureData->Texture2DResource && DepthTextureData->SrcData)
			{
				RHIUpdateTexture2D(
					DepthTextureData->Texture2DResource->GetTexture2DRHI(),
					DepthTextureData->MipIndex, 
					DepthTextureData->Region, 
					DepthTextureData->SrcPitch,
					DepthTextureData->SrcData
				);
			}
			else
			{
				UE_LOG(LogNiPlugin, Error, TEXT("UpdateTextureRegionsCmd: Invalid DepthTextureData"));
			}
		});
	}
	else
	{
		//UE_LOG(LogNiPlugin, Error, TEXT("DepthTexture->Resource is NULL"));
	}

	return true;
}

void FNiContext::ComputeDepthHist(openni::VideoFrameRef& Frame)
{
	const int Width = Frame.getWidth();
	const int Height = Frame.getHeight();
	const int RestOfRow = Frame.getStrideInBytes() / sizeof(openni::DepthPixel) - Width;

	const openni::DepthPixel* pDepth = (const openni::DepthPixel*)Frame.getData();

	FMemory::Memset(DepthHist.GetData(), 0, MAX_DEPTH * sizeof(float));

	unsigned int NumberOfPoints = 0;
	for (int y = 0; y < Height; ++y)
	{
		for (int x = 0; x < Width; ++x, ++pDepth)
		{
			if (*pDepth != 0)
			{
				DepthHist[*pDepth]++;
				NumberOfPoints++;
			}
		}
		pDepth += RestOfRow;
	}

	for (int i = 1; i < MAX_DEPTH; i++)
	{
		DepthHist[i] += DepthHist[i - 1];
	}

	if (NumberOfPoints)
	{
		for (int i = 1; i < MAX_DEPTH; i++)
		{
			DepthHist[i] = (unsigned int)(256 * (1.0f - (DepthHist[i] / NumberOfPoints)));
		}
	}
}

void FNiContext::FillDepthBuffer(nite::UserTrackerFrameRef& Frame, openni::VideoFrameRef& DepthFrame)
{
	const nite::UserMap& UserLabels = Frame.getUserMap();
	const nite::UserId* pLabels = UserLabels.getPixels();

	const openni::DepthPixel* pDepthRow = (const openni::DepthPixel*)DepthFrame.getData();

	FMemory::Memset(DepthBuffer.GetData(), 0, DepthWidth * DepthHeight * sizeof(FPixel_R8G8B8A8));
	FPixel_R8G8B8A8* pTexRow = (FPixel_R8G8B8A8*)DepthBuffer.GetData();

	const int RowSize = DepthFrame.getStrideInBytes() / sizeof(openni::DepthPixel);

	const float Colors[][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {1, 1, 1}};
	const int ColorCount = 3;

	float Factor[3] = {1, 1, 1};

	for (int y = 0; y < DepthFrame.getHeight(); ++y)
	{
		const openni::DepthPixel* pDepth = pDepthRow;
		FPixel_R8G8B8A8* pTex = pTexRow;

		for (int x = 0; x < DepthFrame.getWidth(); ++x, ++pDepth, ++pTex, ++pLabels)
		{
			if (*pDepth != 0)
			{
				if (*pLabels == 0)
				{
					//if (!DrawBackground)
					//{
						//factor[0] = factor[1] = factor[2] = 0;
					//}
					//else
					{
						Factor[0] = Colors[ColorCount][0];
						Factor[1] = Colors[ColorCount][1];
						Factor[2] = Colors[ColorCount][2];
					}
				}
				else
				{
					Factor[0] = Colors[*pLabels % ColorCount][0];
					Factor[1] = Colors[*pLabels % ColorCount][1];
					Factor[2] = Colors[*pLabels % ColorCount][2];
				}

				const float HistValue = DepthHist[*pDepth];
				pTex->R = HistValue * Factor[0];
				pTex->G = HistValue * Factor[1];
				pTex->B = HistValue * Factor[2];
				pTex->A = 0;

				Factor[0] = Factor[1] = Factor[2] = 1;
			}
		}

		pDepthRow += RowSize;
		pTexRow += DepthWidth;
	}
}

//=============================================================================
// UpdateUsers
//=============================================================================

static UNiUser* FindUser(const TArray<UNiUser*>& Data, int Id, int& OutArrayId)
{
	for (int i = 0; i < Data.Num(); ++i)
	{
		if (Data[i]->Id == Id)
		{
			OutArrayId = i;
			return Data[i];
		}
	}

	return nullptr;
}

bool FNiContext::UpdateUsers(nite::UserTrackerFrameRef& Frame)
{
	UNiSensor* Sensor = GetSensor();
	const nite::Array<nite::UserData>& Users = Frame.getUsers();

	for (int i = 0; i < Users.getSize(); ++i)
	{
		const nite::UserData& User = Users[i];
		const int UserId = User.getId();

		// init user
		int UserArrayId = -1;
		UNiUser* UserObj = FindUser(Sensor->Users, UserId, UserArrayId);
		if (!UserObj)
		{
			UserObj = NewObject<UNiUser>(Sensor);
			UserObj->Id = UserId;

			const int NumJoints = (int)ENiJointType::JOINT_UNKNOWN; // JOINT_UNKNOWN last in ENiJointType
			UserObj->Joints.Init(nullptr, NumJoints);

			for (int i = 0; i < NumJoints; ++i)
			{
				UNiJoint* JointObj = NewObject<UNiJoint>(UserObj);
				JointObj->Type = (ENiJointType)i;
				UserObj->Joints[i] = JointObj;
			}

			UserArrayId = Sensor->Users.Add(UserObj);

			UE_LOG(LogNiPlugin, Warning, TEXT("Add user: Id=%d ArrayId=%d"), UserId, UserArrayId);
		}
		else if (User.isNew())
		{
			UE_LOG(LogNiPlugin, Error, TEXT("User already exists: Id=%d ArrayId=%d"), UserId, UserArrayId);
		}

		// update user state
		UserObj->IsNew = User.isNew();
		UserObj->IsVisible = User.isVisible();
		UserObj->IsLost = User.isLost();

		if (User.getSkeleton().getState() == nite::SKELETON_TRACKED)
		{
			UpdateSkeleton(Sensor, User, UserObj);
			UserObj->IsTracking = true;
		}
		else
		{
			UserObj->IsTracking = false;
		}

		UpdatePose(Sensor, User, UserObj);

		// events
		if (User.isNew())
		{
			Sensor->OnNewUser().ExecuteIfBound(UserObj);
			UserTracker->startSkeletonTracking(UserId);
		}

		if (User.isLost())
		{
			Sensor->OnLostUser().ExecuteIfBound(UserObj);

			UE_LOG(LogNiPlugin, Warning, TEXT("Remove user: Id=%d ArrayId=%d"), UserId, UserArrayId);
			Sensor->Users.RemoveAt(UserArrayId);
		}
	}

	return true;
}

void FNiContext::UpdateSkeleton(UNiSensor* Sensor, const nite::UserData& User, UNiUser* UserObj)
{
	{
		// Real World Coordinates, mm
		const nite::Point3f& pos = User.getCenterOfMass();
		UserObj->CenterOfMass = FVector(pos.z, pos.x, pos.y) * 0.1f; // mm -> cm
	}

	{
		// Projective Coordinates, pixels
		const nite::BoundingBox& bb = User.getBoundingBox();
		UserObj->BbMin = FVector(bb.min.x, bb.min.y, bb.min.z);
		UserObj->BbMax = FVector(bb.max.x, bb.max.y, bb.max.z);
	}

	const nite::Skeleton& skel = User.getSkeleton();

	for (int i = 0; i <= (int)nite::JOINT_RIGHT_FOOT; ++i) // JOINT_RIGHT_FOOT last in nite::JointType
	{
		const nite::SkeletonJoint& Joint = skel.getJoint((nite::JointType)i);
		const ENiJointType Type = Utils::ConvertJointType((nite::JointType)i);

		if (Type != ENiJointType::JOINT_UNKNOWN)
		{
			UNiJoint* JointObj = UserObj->Joints[(int)Type];

			// Real World Coordinates, mm
			const nite::Point3f& pos = Joint.getPosition();
			JointObj->Position = FVector(pos.z, pos.x, pos.y) * 0.1f; // mm -> cm
			JointObj->PositionConfidence = Joint.getPositionConfidence();

			const nite::Quaternion& q = Joint.getOrientation();
			JointObj->Orientation = FQuat(q.z, q.x, q.y, q.w).GetNormalized().Rotator();
			JointObj->OrientationConfidence = Joint.getOrientationConfidence();
		}
	}
}

void FNiContext::UpdatePose(UNiSensor* Sensor, const nite::UserData& User, class UNiUser* UserObj)
{
	for (int i = 0; i <= (int)nite::POSE_CROSSED_HANDS; ++i) // POSE_CROSSED_HANDS last in nite::PoseType
	{
		const nite::PoseData& Pose = User.getPose((nite::PoseType)i);
		const ENiPoseType PoseType = Utils::ConvertPoseType((nite::PoseType)i);

		if (Pose.isEntered() && (UserObj->Pose != PoseType))
		{
			UserObj->OldPose = UserObj->Pose;
			UserObj->Pose = PoseType;

			UE_LOG(LogNiPlugin, Warning, TEXT("Enter user pose %d -> %d"), (int)UserObj->OldPose, (int)PoseType);
			Sensor->OnEnterPose().ExecuteIfBound(UserObj);
		}

		if (Pose.isExited() && (UserObj->Pose == PoseType))
		{
			UserObj->OldPose = UserObj->Pose;
			UserObj->Pose = ENiPoseType::POSE_UNKNOWN;

			UE_LOG(LogNiPlugin, Warning, TEXT("Exit user pose %d -> %d"), (int)UserObj->OldPose, (int)PoseType);
			Sensor->OnExitPose().ExecuteIfBound(UserObj);
		}
	}
}
