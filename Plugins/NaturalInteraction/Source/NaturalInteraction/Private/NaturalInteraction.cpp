#include "NiPCH.h"
#include "NaturalInteraction.h"

#include "NiContext.h"
#include "NiLog.h"

DEFINE_LOG_CATEGORY(LogNiPlugin);

FNaturalInteraction::FNaturalInteraction() : 
	Context(nullptr)
{
}

FNaturalInteraction::~FNaturalInteraction()
{
	ReleaseContext();
}

void FNaturalInteraction::StartupModule()
{
	UE_LOG(LogNiPlugin, Warning, TEXT("StartupModule"));
	InitContext();
}

void FNaturalInteraction::ShutdownModule()
{
	UE_LOG(LogNiPlugin, Warning, TEXT("ShutdownModule"));
	ReleaseContext();
}

FNiContext* FNaturalInteraction::GetContext()
{
	return Context;
}

// Internals

void FNaturalInteraction::InitContext()
{
	ReleaseContext();
	Context = new FNiContext();
}

void FNaturalInteraction::ReleaseContext()
{
	if (Context)
	{
		delete Context;
		Context = nullptr;
	}
}

IMPLEMENT_MODULE(FNaturalInteraction, NaturalInteraction)
