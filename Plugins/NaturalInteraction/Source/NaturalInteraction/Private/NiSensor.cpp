#include "NiPCH.h"
#include "NiSensor.h"

#include "NaturalInteraction.h"
#include "NiContext.h"
#include "NiLog.h"

UNiSensor::UNiSensor(const FObjectInitializer &ObjectInitializer) : 
	UObject(ObjectInitializer),
	Context(nullptr) // managed by FNiContext
{
	UE_LOG(LogNiPlugin, Warning, TEXT("NEW UNiSensor"));
}

UNiSensor::~UNiSensor()
{
	UE_LOG(LogNiPlugin, Warning, TEXT("DEL UNiSensor"));
	Context = nullptr;
}

// Static

UNiSensor* UNiSensor::GetSensorInstance()
{
	auto Context = INaturalInteraction::Get().GetContext();
	return Context->GetSensor();
}

int32 UNiSensor::GetMaxJoints()
{
	return (int32)ENiJointType::JOINT_UNKNOWN;
}

int32 UNiSensor::GetMaxPoses()
{
	return (int32)ENiPoseType::POSE_UNKNOWN;
}

// Instance

bool UNiSensor::InitDevice()
{
	return Context->InitDevice();
}

void UNiSensor::ShutdownDevice()
{
	Context->ShutdownDevice();
}

bool UNiSensor::IsDeviceReady()
{
	return (Context && Context->IsDeviceReady());
}

bool UNiSensor::StartTracking()
{
	return Context->StartTracking();
}

void UNiSensor::StopTracking()
{
	Context->StopTracking();
}

bool UNiSensor::StartPoseDetection(UNiUser* User, ENiPoseType Pose)
{
	return Context->StartPoseDetection(User, Pose);
}

void UNiSensor::StopPoseDetection(UNiUser* User, ENiPoseType Pose)
{
	Context->StopPoseDetection(User, Pose);
}

UTexture2D* UNiSensor::GetDepthTexture()
{
	return Context->GetDepthTexture();
}
