#include "NiPCH.h"
#include "NiUser.h"

UNiUser::UNiUser(const FObjectInitializer &ObjectInitializer) : 
	UObject(ObjectInitializer),
	Pose(ENiPoseType::POSE_UNKNOWN),
	OldPose(ENiPoseType::POSE_UNKNOWN)
{
}

UNiUser::~UNiUser()
{
}

UNiJoint* UNiUser::GetJoint(ENiJointType Type)
{
	const int Id = (int)Type;

	if (Id >= 0 && Id < Joints.Num())
	{
		return Joints[Id];
	}

	return nullptr;
}
