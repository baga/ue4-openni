using UnrealBuildTool;
using System.IO;
 
public class NaturalInteraction : ModuleRules
{
    public NaturalInteraction(TargetInfo Target)
    {
        UEBuildConfiguration.bForceEnableExceptions = true;

        PrivateIncludePaths.AddRange(new string[] { "NaturalInteraction/Private" });
        PublicIncludePaths.AddRange(new string[] { "NaturalInteraction/Public" });
 
        PublicDependencyModuleNames.AddRange(
		    new string[] { 
				"Core", 
				"CoreUObject", 
				"Engine", 
				"InputCore",			
				"RHI",
				"RenderCore",
				"UMG",
				"Slate",
				"SlateCore",
				"PhysX"
			}
		);

        PublicIncludePaths.Add(Path.Combine(ThirdPartyPath, "OpenNI/include/"));
        PublicAdditionalLibraries.Add(Path.Combine(ThirdPartyPath, "OpenNI/lib/OpenNI2.lib"));
        PublicAdditionalLibraries.Add(Path.Combine(ThirdPartyPath, "OpenNI/lib/NiTE2.lib"));

        // https://answers.unrealengine.com/questions/26463/loading-dlls-from-plugins-1.html

        string BinDir = Path.Combine(ThirdPartyPath, "OpenNI/bin");

        //if (Target.Platform == UnrealTargetPlatform.Win64)
        {
            RuntimeDependencies.Add(new RuntimeDependency(Path.Combine(BinDir, "OpenNI2.dll")));
            RuntimeDependencies.Add(new RuntimeDependency(Path.Combine(BinDir, "NiTE2.dll")));
            RuntimeDependencies.Add(new RuntimeDependency(Path.Combine(BinDir, "PS1080.dll")));

            PublicDelayLoadDLLs.Add(Path.Combine(BinDir, "OpenNI2.dll"));
            PublicDelayLoadDLLs.Add(Path.Combine(BinDir, "NiTE2.dll"));
            PublicDelayLoadDLLs.Add(Path.Combine(BinDir, "PS1080.dll"));
        }
    }

    private string ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModuleDirectory, "../../ThirdParty/")); }
    }
}
