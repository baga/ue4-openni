#pragma once

#include "NiComponent.generated.h"

UCLASS(ClassGroup=Input, meta=(BlueprintSpawnableComponent))
class NATURALINTERACTION_API UNiComponent : public UActorComponent
{
	GENERATED_UCLASS_BODY()

public:

	virtual ~UNiComponent();
	
	virtual void OnRegister() override;
	virtual void OnUnregister() override;
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

private:

	class FNiContext* Context;
};
