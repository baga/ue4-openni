#pragma once

#include "NiJoint.generated.h"

// ENiJointType
UENUM(BlueprintType)
enum class ENiJointType : uint8
{
	JOINT_HEAD UMETA(DisplayName="Head"),
	JOINT_NECK UMETA(DisplayName="Neck"),
	JOINT_TORSO UMETA(DisplayName="Torso"),

	JOINT_LEFT_SHOULDER UMETA(DisplayName="Left Shoulder"),
	JOINT_RIGHT_SHOULDER UMETA(DisplayName="Right Shoulder"),
	JOINT_LEFT_ELBOW UMETA(DisplayName="Left Elbow"),
	JOINT_RIGHT_ELBOW UMETA(DisplayName="Right Elbow"),
	JOINT_LEFT_HAND UMETA(DisplayName="Left Hand"),
	JOINT_RIGHT_HAND UMETA(DisplayName="Right Hand"),

	JOINT_LEFT_HIP UMETA(DisplayName="Left Hip"),
	JOINT_RIGHT_HIP UMETA(DisplayName="Right Hip"),
	JOINT_LEFT_KNEE UMETA(DisplayName="Left Knee"),
	JOINT_RIGHT_KNEE UMETA(DisplayName="Right Knee"),
	JOINT_LEFT_FOOT UMETA(DisplayName="Left Foot"),
	JOINT_RIGHT_FOOT UMETA(DisplayName="Right Foot"),

	JOINT_UNKNOWN UMETA(DisplayName="Unknown") // LAST
};

// UNiJoint
UCLASS(BlueprintType)
class NATURALINTERACTION_API UNiJoint : public UObject
{
	GENERATED_UCLASS_BODY()

public:

	~UNiJoint();

	// Props

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiJoint")
	ENiJointType Type;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiJoint")
	FVector Position;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiJoint")
	float PositionConfidence;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiJoint")
	FRotator Orientation;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiJoint")
	float OrientationConfidence;
};
