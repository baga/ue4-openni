#pragma once

#include "NiUser.h"
#include "NiSensor.generated.h"

DECLARE_DYNAMIC_DELEGATE_OneParam(FNiUserDelegate, UNiUser*, User);

#define NI_DELEGATE_IMPL(DelegType, Name)\
	DelegType Name##Event;\
	virtual DelegType& On##Name() { return Name##Event; }

// UNiSensor
UCLASS(BlueprintType)
class NATURALINTERACTION_API UNiSensor : public UObject
{
	GENERATED_UCLASS_BODY()

public:

	~UNiSensor();

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	static UNiSensor* GetSensorInstance();

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	static int32 GetMaxJoints();

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	static int32 GetMaxPoses();

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	bool InitDevice();

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	void ShutdownDevice();

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	bool IsDeviceReady();

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	bool StartTracking();

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	void StopTracking();

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	bool StartPoseDetection(UNiUser* User, ENiPoseType Pose);

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	void StopPoseDetection(UNiUser* User, ENiPoseType Pose);

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	UTexture2D* GetDepthTexture();

	// Delegates

	NI_DELEGATE_IMPL(FNiUserDelegate, NewUser);
	NI_DELEGATE_IMPL(FNiUserDelegate, LostUser);
	NI_DELEGATE_IMPL(FNiUserDelegate, EnterPose);
	NI_DELEGATE_IMPL(FNiUserDelegate, ExitPose);

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	void SubscribeNewUser(FNiUserDelegate InDelegate) { NewUserEvent = InDelegate; }

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	void SubscribeLostUser(FNiUserDelegate InDelegate) { LostUserEvent = InDelegate; }

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	void SubscribeEnterPose(FNiUserDelegate InDelegate) { EnterPoseEvent = InDelegate; }

	UFUNCTION(BlueprintCallable, Category = "NiSensor")
	void SubscribeExitPose(FNiUserDelegate InDelegate) { ExitPoseEvent = InDelegate; }

	// Props

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiSensor")
	TArray<UNiUser*> Users;

private:

	friend class FNiContext;
	class FNiContext* Context;
};
