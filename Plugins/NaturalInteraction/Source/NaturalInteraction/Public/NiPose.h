#pragma once

#include "NiPose.generated.h"

// ENiPoseType
UENUM(BlueprintType)
enum class ENiPoseType : uint8
{
	POSE_PSI UMETA(DisplayName="PSI"),
	POSE_CROSSED_HANDS UMETA(DisplayName="Crossed Hands"),

	POSE_UNKNOWN UMETA(DisplayName="Unknown") // LAST
};
