#pragma once

#include "ModuleManager.h"

class NATURALINTERACTION_API INaturalInteraction : public IModuleInterface
{
public:

	static inline INaturalInteraction& Get()
	{
		return FModuleManager::LoadModuleChecked<INaturalInteraction>("NaturalInteraction");
	}

	static inline bool IsAvailable()
	{
		return FModuleManager::Get().IsModuleLoaded("NaturalInteraction");
	}

	virtual class FNiContext* GetContext() = 0;
};
