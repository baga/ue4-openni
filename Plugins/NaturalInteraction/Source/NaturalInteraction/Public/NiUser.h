#pragma once

#include "NiJoint.h"
#include "NiPose.h"
#include "NiUser.generated.h"

// UNiUser
UCLASS(BlueprintType)
class NATURALINTERACTION_API UNiUser : public UObject
{
	GENERATED_UCLASS_BODY()

public:

	~UNiUser();

	UFUNCTION(BlueprintCallable, Category = "NiUser")
	UNiJoint* GetJoint(ENiJointType Type);

	// Props

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiUser")
	int32 Id;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiUser")
	bool IsNew;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiUser")
	bool IsVisible;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiUser")
	bool IsLost;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiUser")
	bool IsTracking;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiUser")
	FVector CenterOfMass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiUser")
	FVector BbMin;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiUser")
	FVector BbMax;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiUser")
	TArray<UNiJoint*> Joints;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiUser")
	ENiPoseType Pose;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "NiUser")
	ENiPoseType OldPose;
};
